/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import rpgfx.RpgFX;

/**
 * FXML Controller class
 *
 * @author Luish
 */
public class TelaLoginController implements Initializable
{

    @FXML
    private JFXTextField txuser;
    @FXML
    private JFXPasswordField txsenha;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
        txuser.setText("admin");
        txsenha.setText("admin");
    }    

    @FXML
    private void evtLogin(MouseEvent event)
    {
        try
        {
            Parent root = FXMLLoader.load(getClass().getResource("TelaPrincipal.fxml"));
            
            Scene scene = new Scene(root);
            scene.getStylesheets().add("/style/style.css");
            RpgFX.getSTAGE().setScene(scene);
            RpgFX.getSTAGE().setMaximized(true);
            
        } catch (IOException ex)
        {
            System.out.println(ex.getMessage());
        }
    }

    @FXML
    private void evtCancel(MouseEvent event)
    {
        RpgFX.getSTAGE().close();
        System.exit(1);
    }

    @FXML
    private void evtTeclaPressionada(KeyEvent event)
    {
        if(event.getCode() == KeyCode.ENTER)
        {
            evtLogin(null);
        }
    }
    
}

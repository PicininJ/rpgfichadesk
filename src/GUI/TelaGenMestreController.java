/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import AbstractOrInterface.CtrUsuario;
import Util.util;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author Luish
 */
public class TelaGenMestreController implements Initializable
{

    @FXML
    private JFXTextField txcodigo;
    @FXML
    private JFXTextField txnome;
    @FXML
    private DatePicker dtcadastro;
    @FXML
    private JFXTextField txemail;
    @FXML
    private JFXPasswordField txPassword;
    @FXML
    private JFXButton btNovo;
    @FXML
    private JFXButton btAlterar;
    @FXML
    private JFXButton btExcluir;
    @FXML
    private JFXButton btConfirmar;
    @FXML
    private JFXButton btCancelar;
    @FXML
    private AnchorPane pndados;
    @FXML
    private TableColumn<Object, Integer> colCod;
    @FXML
    private TableColumn<Object, String> colNome;
    @FXML
    private TableColumn<Object, Date> colDtCadastro;
    @FXML
    private TableColumn<Object, String> colEmail;
    @FXML
    private TableView<Object> tabela;

    private static CtrUsuario ctrMestre;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
        iniciaComponentes();
    }

    @FXML
    private void evtNovo(MouseEvent event)
    {
        EstadoEdicao();
    }

    @FXML
    private void evtAlterar(MouseEvent event)
    {
        EstadoEdicao();
    }

    @FXML
    private void evtExcluir(MouseEvent event)
    {
        ctrMestre.deleteCtr(Integer.parseInt(txcodigo.getText()));
    }

    @FXML
    private void evtConfirmar(MouseEvent event)
    {
        if (txcodigo.getText().isEmpty())
        {
            ctrMestre.insertCtr();
        } else
        {
            ctrMestre.updateCtr();
        }
        EstadoOriginal();
    }

    @FXML
    private void evtCancelar(MouseEvent event)
    {
        EstadoOriginal();
    }

    private void iniciaComponentes()
    {
        colCod.setCellValueFactory(new PropertyValueFactory<>("codigo"));
        colDtCadastro.setCellValueFactory(new PropertyValueFactory<>("dtCadastro"));
        colEmail.setCellValueFactory(new PropertyValueFactory<>("email"));
        colNome.setCellValueFactory(new PropertyValueFactory<>("nome"));
        
        ctrMestre.setALL(txcodigo, txnome, dtcadastro, txemail, txPassword, tabela);
        EstadoOriginal();
    }

    private void EstadoOriginal()
    {
        pndados.setDisable(true);
        util.limpaPane(pndados);
        dtcadastro.setValue(LocalDate.now());
        CarregaTabela();
    }

    private void EstadoEdicao()
    {
        pndados.setDisable(false);
    }

    @FXML
    private void evtClickTabela(MouseEvent event)
    {
        ctrMestre.setCampos();
    }

    public static void setCtrMestre(CtrUsuario ctrMestre)
    {
        TelaGenMestreController.ctrMestre = ctrMestre;
    }

    private void CarregaTabela()
    {
        ctrMestre.carregaTabela();
    }

}

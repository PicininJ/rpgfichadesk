/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import AbstractOrInterface.CtrInformacao;
import AbstractOrInterface.CtrUsuario;
import Controladoras.CtrAntecedente;
import Controladoras.CtrDesvantagem;
import Controladoras.CtrHabilidade;
import Controladoras.CtrItem;
import Controladoras.CtrMestre;
import Controladoras.CtrRaca;
import Controladoras.CtrTipo;
import Controladoras.CtrVantagem;
import Util.Musica;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXToggleButton;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.media.MediaPlayer;

/**
 *
 * @author Luish
 */
public class TelaPrincipalController implements Initializable
{

    @FXML
    private HBox pndado;

    private ObservableList<Musica> musicas;
    @FXML
    private JFXComboBox<Musica> cbsons;
    @FXML
    private JFXButton btnPlay;
    @FXML
    private JFXToggleButton tbAutoPlay;
    @FXML
    private FontAwesomeIconView iconePlayPause;

    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
        CarregaMusicas();
    }

    @FXML
    private void evtHome(MouseEvent event)
    {
        pndado.getChildren().clear();
    }

    @FXML
    private void evtTocaPausaSom(MouseEvent event)
    {
        Musica m = cbsons.getSelectionModel().getSelectedItem();
        if (m != null)
        {
            if (m.getAudio().getStatus() == MediaPlayer.Status.PLAYING)
            {
                m.getAudio().pause();
                btnPlay.setText("Play");
                iconePlayPause.setIcon(FontAwesomeIcon.PLAY);
            } else
            {
                m.getAudio().play();
                btnPlay.setText("Pause");
                iconePlayPause.setIcon(FontAwesomeIcon.PAUSE);
            }
        }
    }

    @FXML
    private void evtPararSom(MouseEvent event)
    {
        Musica m = cbsons.getSelectionModel().getSelectedItem();
        if (m != null)
        {
            m.getAudio().stop();
            btnPlay.setText("Play");
            iconePlayPause.setIcon(FontAwesomeIcon.PLAY);
        }
    }

    @FXML
    private void evtSetLoop(MouseEvent event)
    {
        for (int i = 0; i < cbsons.getItems().size(); i++)
        {
            cbsons.getItems().get(i).autoLoop(tbAutoPlay.isSelected());
        }
    }

    /**
     * método que irá escaneaqr a pasta de audio e careggará os seus arquivos
     * .mp3 na interface para sua posterior reprodução.
     */
    private void CarregaMusicas()
    {
        musicas = FXCollections.observableArrayList();
        try
        {
            File f = new File("sons/");
            if (f != null && f.isDirectory())
            {
                File[] fv = f.listFiles();
                for (int i = 0; i < fv.length; i++)
                {
                    if (fv[i].getName().endsWith(".mp3"))
                    {
                        musicas.add(new Musica(fv[i]));
                    }
                }
                cbsons.setItems(musicas);
            }
            //musicas.add(new Musica(new File("C://Users//Luish//Documents//NetBeansProjects//RpgFX//sons//Brook song YO HO HO HO.mp3")));
        } catch (Exception ex)
        {
            System.out.println(ex.getMessage());
        }
    }

    /**
     *
     * @param nt Caminho para o FXML(se estiver na mesma pasta é só o nome)
     * @param controladora Instancia da controladora que será responsável pela
     * tela
     * @return
     */
    private void CarregaSubTela(String nt, Object... controladora)
    {
        pndado.getChildren().clear();
        if (controladora.length > 0)
        {
            if (controladora[0] instanceof CtrInformacao)
            {
                TelaInformacoesController.setCtrinformacao((CtrInformacao) controladora[0]);
            } else if (controladora[0] instanceof CtrUsuario)
            {
                TelaGenMestreController.setCtrMestre((CtrUsuario) controladora[0]);
            }
        }
        try
        {
            Parent p = FXMLLoader.load(getClass().getResource(nt));
            pndado.getChildren().add(p);
        } catch (Exception ex)
        {
            System.out.println(ex.getMessage());
        }
    }

    @FXML
    private void evtVantagem(ActionEvent event)
    {
        CarregaSubTela("TelaInformacoes.fxml", new CtrVantagem());
    }

    @FXML
    private void evtDesvantagem(ActionEvent event)
    {
        CarregaSubTela("TelaInformacoes.fxml", new CtrDesvantagem());
    }

    @FXML
    private void evtTipo(ActionEvent event)
    {
        CarregaSubTela("TelaInformacoes.fxml", new CtrTipo());
    }

    @FXML
    private void evtRaca(ActionEvent event)
    {
        CarregaSubTela("TelaInformacoes.fxml", new CtrRaca());
    }

    @FXML
    private void evtHabilidade(ActionEvent event)
    {
        CarregaSubTela("TelaInformacoes.fxml", new CtrHabilidade());
    }

    @FXML
    private void evtAntecedente(ActionEvent event)
    {
        CarregaSubTela("TelaInformacoes.fxml", new CtrAntecedente());
    }

    @FXML
    private void evtItem(ActionEvent event)
    {
        CarregaSubTela("TelaInformacoes.fxml", new CtrItem());
    }

    @FXML
    private void evtSistema(ActionEvent event)
    {
        CarregaSubTela("TelaSistema.fxml");
    }
    @FXML
    private void evtMestre(ActionEvent event)
    {
        CarregaSubTela("TelaGenMestre.fxml", new CtrMestre());
    }

    @FXML
    private void evtJogador(ActionEvent event)
    {
        CarregaSubTela("TelaFicha.fxml");
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import Controladoras.CtrSistemaRPG;
import Util.util;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author Luish
 */
public class TelaSistemaController implements Initializable
{

    @FXML
    private JFXTextField txparametro;
    @FXML
    private JFXComboBox<Object> cbchave;
    @FXML
    private TableColumn<Object, Integer> colcod;
    @FXML
    private TableColumn<Object, String> colnome;
    @FXML
    private TableColumn<Object, String> coldescricao;
    @FXML
    private JFXTextField txcodigo;
    @FXML
    private JFXTextField txnome;
    @FXML
    private JFXTextArea txdescricao;
    @FXML
    private AnchorPane pndados;
    @FXML
    private TableView<Object> tabela;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
        iniciaComponentes();
    }

    @FXML
    private void evtBusca(MouseEvent event)
    {
    }

    @FXML
    private void evtnovo(MouseEvent event)
    {
        estadoEdicao();
    }

    @FXML
    private void evtAlterar(MouseEvent event)
    {
        estadoEdicao();
    }

    @FXML
    private void evtExcluir(MouseEvent event)
    {
        CtrSistemaRPG.deleteCtr(Integer.parseInt(txcodigo.getText()));
        estadoOriginal();
    }

    @FXML
    private void evtCancelar(MouseEvent event)
    {
        util.limpaPane(pndados);
    }

    private void iniciaComponentes()
    {
        colcod.setCellValueFactory(new PropertyValueFactory<>("codigo"));
        colnome.setCellValueFactory(new PropertyValueFactory<>("nome"));
        coldescricao.setCellValueFactory(new PropertyValueFactory<>("descricao"));
        estadoOriginal();
        
    }

    @FXML
    private void evtConfirmar(MouseEvent event)
    {
        if(txcodigo.getText().isEmpty())//novo
        {
            CtrSistemaRPG.insertCtr(txnome.getText(), txdescricao.getText());
        }
        else
        {
            CtrSistemaRPG.updateCtr(Integer.parseInt(txcodigo.getText()), txnome.getText(), txdescricao.getText());
        }
        estadoOriginal();
    }

    private void carregaTabela(String string)
    {
        tabela.setItems(FXCollections.observableArrayList(CtrSistemaRPG.getListaCtr()));
    }

    @FXML
    private void evtClickTabela(MouseEvent event)
    {
        CtrSistemaRPG.preencheCampos(txcodigo, txnome, txdescricao, tabela.getSelectionModel().getSelectedItem());
    }

    private void estadoOriginal()
    {
        pndados.setDisable(true);
        util.limpaPane(pndados);
        carregaTabela("");
    }
    private void estadoEdicao()
    {
        pndados.setDisable(false);
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import AbstractOrInterface.CtrInformacao;
import Banco.Banco;
import Controladoras.CtrDesvantagem;
import Controladoras.CtrItem;
import Controladoras.CtrSistemaRPG;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;

/**
 * FXML Controller class
 *
 * @author Luish
 */
public class TelaInformacoesController implements Initializable
{

    @FXML
    private JFXButton btadicionar;
    @FXML
    private JFXButton btalterar;
    @FXML
    private JFXButton btExcluir;
    @FXML
    private JFXTextField txcodigo;
    @FXML
    private JFXTextField txnome;
    @FXML
    private JFXTextArea txdescricao;
    @FXML
    private JFXButton btconfirmar;
    @FXML
    private JFXButton btcancelar;
    @FXML
    private JFXButton btbusca;
    @FXML
    private VBox pncampos;
    @FXML
    private TableView<Object> tabela;
    @FXML
    private TableColumn<Object, Integer> colcodigo;
    @FXML
    private TableColumn<Object, String> colnome;
    @FXML
    private TableColumn<Object, String> coldescricao;
    @FXML
    private JFXTextField txchave;
    @FXML
    private JFXComboBox<String> cbfiltro;
    @FXML
    private JFXComboBox<Object> cbSistema;
    @FXML
    private TableColumn<Object, Object> colsistema;

    private static CtrInformacao ctrinformacao;
    @FXML
    private JFXComboBox<Object> cbTipo;
    @FXML
    private Spinner<Integer> spraridade;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
        try
        {
            inicia_componentes();
        } catch (Exception ex)
        {
            System.out.println(ex.getMessage());
        }
    }

    @FXML
    private void evtAdicionaInformacao(MouseEvent event)
    {
        estadoEdicao();
    }

    @FXML
    private void evtAlterarInformacao(MouseEvent event)
    {
        estadoEdicao();
    }

    @FXML
    private void evtExcluirInformacao(MouseEvent event)
    {
        Alert a = new Alert(Alert.AlertType.CONFIRMATION);
        a = new Alert(Alert.AlertType.CONFIRMATION, "Deseja realmente apagar este dado", ButtonType.YES, ButtonType.NO);
        a.setContentText("Deseja realmente apagar este dado");
        if (a.showAndWait().get() == ButtonType.YES)
        {
            if (ctrinformacao.deleteCtr(Integer.parseInt(txcodigo.getText())))
            {
                a = new Alert(Alert.AlertType.INFORMATION);
                a.setContentText("Informação apagada com sucesso!!!");
                estadoOriginal();
            } else
            {
                a = new Alert(Alert.AlertType.ERROR);
                a.setContentText("Erro: " + Banco.getConexao().getMensagemErro());
            }
        } else
        {
            a = new Alert(Alert.AlertType.WARNING, "Operação Cancelada!!!");
        }
        a.show();
        estadoOriginal();
    }

    @FXML
    private void evtconfirmar(MouseEvent event)
    {
        Alert a = null;
        try
        {
            if (validaCampos())
            {
                if (txcodigo.getText().isEmpty())
                {
                    if (ctrinformacao.insertCtr(cbSistema.getSelectionModel().getSelectedItem(), 
                            cbTipo.getSelectionModel().getSelectedItem(),
                            txnome.getText(), txdescricao.getText(), 
                            spraridade.getValue().toString()))
                    {
                        a = new Alert(Alert.AlertType.INFORMATION);
                        a.setContentText("Informação inserida com sucesso!!!");
                        estadoOriginal();
                    } else
                    {
                        a = new Alert(Alert.AlertType.ERROR);
                        a.setContentText("Erro: " + Banco.getConexao().getMensagemErro());
                    }
                } else
                {
                    if (ctrinformacao.updateCtr(Integer.parseInt(txcodigo.getText()), 
                            cbSistema.getSelectionModel().getSelectedItem(),
                            cbTipo.getSelectionModel().getSelectedItem(),
                            txnome.getText(), txdescricao.getText(), 
                            spraridade.getValue().toString()))
                    {
                        a = new Alert(Alert.AlertType.INFORMATION);
                        a.setContentText("Informação atualizada com sucesso!!!");
                        estadoOriginal();
                    } else
                    {
                        a = new Alert(Alert.AlertType.ERROR);
                        a.setContentText("Erro: " + Banco.getConexao().getMensagemErro());
                    }
                }
            }
        } catch (Exception ex)
        {
            a = new Alert(Alert.AlertType.ERROR);
            a.setContentText(ex.getMessage());
        }

        a.show();
        estadoOriginal();
    }

    @FXML
    private void evtcancelar(MouseEvent event)
    {
        estadoOriginal();
    }

    @FXML
    private void evtClickNaTabela(MouseEvent event)
    {
        Object o = tabela.getSelectionModel().getSelectedItem();
        //ctrinformacao.setCampos(cbSistema, o, txdescricao, txcodigo, txnome);
        ctrinformacao.setCampos(cbSistema, cbTipo, spraridade, o, txdescricao, txcodigo, txnome);
        //estadoEdicao();
        btadicionar.setDisable(true);
        btalterar.setDisable(false);
        btExcluir.setDisable(false);
    }

    private void estadoOriginal()
    {
        pncampos.setDisable(true);
        btExcluir.setDisable(true);
        btadicionar.setDisable(false);
        btalterar.setDisable(true);
        btcancelar.setDisable(true);
        btconfirmar.setDisable(true);
        tabela.setDisable(false);
        cbSistema.setItems(FXCollections.observableArrayList(CtrSistemaRPG.getListaCtr()));

        txchave.setText("");
        txcodigo.setText("");
        txdescricao.setText("");
        txnome.setText("");
        carregaTabela();
    }

    private void estadoEdicao()
    {
        pncampos.setDisable(false);
        btExcluir.setDisable(true);
        btadicionar.setDisable(false);
        btalterar.setDisable(false);
        btcancelar.setDisable(false);
        btconfirmar.setDisable(false);
        tabela.setDisable(true);
    }

    private void inicia_componentes()
    {
        SpinnerValueFactory<Integer> valueFactory = new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 10, 0);
        
        spraridade.setValueFactory(valueFactory);
        colnome.setCellValueFactory(new PropertyValueFactory<>("nome"));
        coldescricao.setCellValueFactory(new PropertyValueFactory<>("descricao"));
        colcodigo.setCellValueFactory(new PropertyValueFactory<>("codigo"));
        colsistema.setCellValueFactory(new PropertyValueFactory<>("sistema"));
        if(ctrinformacao instanceof CtrItem)
            cbTipo.setItems(CtrItem.getTiposItens());
        //colvalor.setCellValueFactory(new PropertyValueFactory<>("valor"));

        //ctrinformacao = new CtrDesvantagem();//trocar aquiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii
        estadoOriginal();

    }

    private void carregaTabela()
    {
        ObservableList<Object> ob = FXCollections.observableArrayList(ctrinformacao.getListaCtr(txchave.getText(), cbfiltro.getSelectionModel().getSelectedItem()));
        tabela.getItems().clear();
        tabela.setItems(ob);
    }

    private boolean validaCampos()
    {
        StringBuilder msg = new StringBuilder("");
        if (cbSistema.getSelectionModel().getSelectedItem() == null)
        {
            msg.append("Sistema Não Selecionado!!!\n");
        }
        if (txnome.getText().isEmpty())
        {
            msg.append("Nome não preenchido!!!\n");
        }
        if (txdescricao.getText().isEmpty())
        {
            msg.append("Descrição não preenchida!!!");
        }
        if (msg.length() != 0)
        {
            Alert a = new Alert(Alert.AlertType.WARNING, msg.toString());
            a.show();
            return false;
        } else
        {
            return true;
        }
    }

    public static CtrInformacao getCtrinformacao()
    {
        return ctrinformacao;
    }

    public static void setCtrinformacao(CtrInformacao ctrinformacao)
    {
        TelaInformacoesController.ctrinformacao = ctrinformacao;
    }
}

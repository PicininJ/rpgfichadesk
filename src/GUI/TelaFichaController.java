/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import Util.JavaKeywordsDemo;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TextInputControl;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import org.fxmisc.richtext.CodeArea;
import org.json.JSONObject;

/**
 * FXML Controller class
 *
 * @author Luish
 */
public class TelaFichaController implements Initializable
{

    @FXML
    private VBox pnDadosEdit;
    @FXML
    private CodeArea codeArea;
    @FXML
    private JFXTextField txexample;
    @FXML
    private Font x321;
    @FXML
    private Font x32;
    @FXML
    private Font x322;
    @FXML
    private Color x4;
    @FXML
    private Font x3;
    @FXML
    private ScrollPane sp;
    @FXML
    private Spinner<Integer> spDinheiro;
    @FXML
    private Spinner<Integer> spHP;
    @FXML
    private Spinner<Integer> spHPMax;
    @FXML
    private Spinner<Integer> spMP;
    @FXML
    private Spinner<Integer> spMPMax;
    @FXML
    private Spinner<Integer> spXP;
    @FXML
    private Spinner<Integer> spLevel;
    @FXML
    private Spinner<Integer> spSorte;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
        iniciaComponentes();
        
        //new JavaKeywordsAsyncDemo().init(codeArea);
        /*new Alert(Alert.AlertType.INFORMATION, "Protótipo de interface:\nJá está "
                + "adicionando itens nos atributos dinâmicos e os removendo caso "
                + "a tecla DELETE seja pressionada enquanto uma caixa de texto"
                + "estiver selecionada", ButtonType.OK).show();*/
    }

    @FXML
    private void evtAdd(MouseEvent event)
    {
        JFXTextField tf = new JFXTextField();
        tf.setMaxWidth(txexample.getMaxWidth());
        tf.setLabelFloat(true);
        tf.setOnKeyPressed(txexample.getOnKeyPressed());
        tf.setPromptText(Integer.toString(pnDadosEdit.getChildren().size()) + ":");
        pnDadosEdit.getChildren().add(tf);

    }

    private void evtRemove()
    {
        Node index = getSelectedItem(pnDadosEdit.getChildren());
        pnDadosEdit.getChildren().remove(index);
        /*JSONObject j = new JSONObject("");
         String[] keys = JSONObject.getNames(j);
         StringBuilder s = new StringBuilder();
         for (int i = 0; i < keys.length; i++)
         s.append(keys[i]);
         new Alert(Alert.AlertType.INFORMATION, s.toString(),  ButtonType.OK).show();*/
    }

    @FXML
    private void evtExibe(MouseEvent event)
    {
        JFXTextField tf;
        StringBuilder s = new StringBuilder();
        for (int i = 0; i < pnDadosEdit.getChildren().size(); i++)
        {
            if (pnDadosEdit.getChildren().get(i) instanceof JFXTextField)
            {
                tf = ((JFXTextField) (pnDadosEdit.getChildren().get(i)));
                s.append(tf.getPromptText()).append(tf.getText()).append("\n");
            }

        }
        new Alert(Alert.AlertType.INFORMATION, s.toString(), ButtonType.OK).show();
    }

    private Node getSelectedItem(ObservableList<Node> l)
    {
        boolean f = false;
        int i;
        for (i = 0; i < l.size() && !f; i++)
        {
            if ((l.get(i) instanceof TextInputControl))
            {
                f = ((TextInputControl) l.get(i)).isFocused();
            }
        }
        return (f) ? l.get(i - 1) : null;
    }

    @FXML
    private void evtTeclaPressionada(KeyEvent event)
    {
        if (event.getCode() == KeyCode.DELETE)
        {
            evtRemove();
        }
    }

    @FXML
    private void evtConvertToJSON(MouseEvent event)
    {
        StringBuilder s = new StringBuilder();
        ObservableList<Node> l = pnDadosEdit.getChildren();

        s.append("{");
        for (int i = 0; i < l.size(); i++)
        {
            s.append("\n \t \"").append(((TextInputControl) (l.get(i))).getPromptText().replaceAll(":", "")).append("\": \"").append(((TextInputControl) (l.get(i))).getText()).append("\" ");
            if (i + 1 < l.size())
            {
                s.append(",");
            } else
            {
                s.append("\n}");
            }
        }
        codeArea.replaceText(s.toString());
        //replaceText
        //codeArea.getTextStyleForInsertionAt(0).add(s.toString());

    }

    @FXML
    private void evtConvertToGUI(MouseEvent event)
    {
        JFXTextField tf;

        JSONObject j = new JSONObject(codeArea.getText());
        String[] keys = JSONObject.getNames(j);
        
        pnDadosEdit.getChildren().clear();
        for (int i = 0; i < keys.length; i++)
        {
            tf = new JFXTextField();
            /*if(i == 0)
                tf.setPadding(new Insets(20, 0, 0, 0));*/
            tf.setMaxWidth(txexample.getMaxWidth());
            tf.setLabelFloat(true);
            tf.setOnKeyPressed(txexample.getOnKeyPressed());
            tf.setPromptText(keys[i] + ":");
            tf.setText(j.getString(keys[i]));
            
            
            pnDadosEdit.getChildren().add(tf);
        }
    }

    private void iniciaComponentes()
    {
        JavaKeywordsDemo.init(codeArea);
        evtConvertToGUI(null);
        
        spDinheiro.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 50, 1, 1));
        spHP.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 50, 1, 1));
        spHPMax.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 50, 1, 1));
        spLevel.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 50, 1, 1));
        spMP.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 50, 1, 1));
        spMPMax.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 50, 1, 1));
        spSorte.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 50, 1, 1));
        spXP.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 50, 1, 1));
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Util;

import java.io.File;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.util.Duration;

/**
 *
 * @author Luish
 */
public class Musica
{

    private MediaPlayer audio;
    private String nome;

    public Musica(File f)
    {
        Media hit = new Media(f.toURI().toString());
        audio = new MediaPlayer(hit);
        nome = f.getName();
    }

    public MediaPlayer getAudio()
    {
        return audio;
    }

    public void setAudio(MediaPlayer audio)
    {
        this.audio = audio;
    }

    public void autoLoop(boolean f)
    {
        audio.setOnEndOfMedia(new Runnable()
        {
            @Override
            public void run()
            {
                if (f)
                {
                    audio.seek(Duration.ZERO);
                }
            }
        });
    }

    @Override
    public String toString()
    {
        return nome;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Util;

import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;

/**
 *
 * @author Luish
 */
public class util
{
    public static void limpaPane(Pane p)
    {
        ObservableList<Node> o = p.getChildren();
        for (int i = 0; i < o.size(); i++)
        {
            if(o.get(i) instanceof TextField)
                ((TextField) o.get(i)).setText("");
            else if(o.get(i) instanceof TextArea)
                ((TextArea) o.get(i)).setText("");
            else if(o.get(i) instanceof ComboBox)
                ((ComboBox) o.get(i)).getSelectionModel().select(null);
            else if(o.get(i) instanceof Pane)
                limpaPane((Pane) o.get(i));
        }
    }
}

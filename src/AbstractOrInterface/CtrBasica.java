/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AbstractOrInterface;

import java.util.ArrayList;

/**
 *
 * @author Luish
 */
public abstract class CtrBasica
{

    public final Boolean insertCtr(Object... o)
    {
        return getInstanciaInformacao(o).exeIncert();
    }

    public final Boolean updateCtr(Object... o)
    {
        return getInstanciaInformacao(o).exeUpdate();
    }

    public final Boolean deleteCtr(Integer codigo)
    {
        return getInstanciaInformacao(codigo).exeDelete();
    }

    public final ArrayList<Object> getListaCtr(String... vf)
    {
        return getInstanciaInformacao().getLista(vf);
    }

    public abstract void setCampos(Object... o);

    protected abstract Basica getInstanciaInformacao(Object... o);
}

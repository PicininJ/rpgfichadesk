/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AbstractOrInterface;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import java.util.ArrayList;
import javafx.scene.control.Spinner;

/**
 *
 * @author Luish
 */
public abstract class CtrInformacao
{

    /**
     *
     * @param sistema sistema da informação
     * @param vs na seguinte ordem \n nome, descricao
     * @return
     */
    public final Boolean insertCtr(Object sistema, Object tipo, String... vs)
    {
        if(sistema != null)
            return getInstanciaInformacao(null, sistema, tipo, vs).exeIncert();
        else
            return false;
    }

    /**
     *
     * @param cod
     * @param sistema sistema da informação
     * @param vs na seguinte ordem \n nome, descricao
     * @return
     */
    public final Boolean updateCtr(Integer cod, Object sistema,Object tipo, String... vs)
    {
        return getInstanciaInformacao(cod, sistema, tipo, vs).exeUpdate();
    }

    /**
     *
     * @param codigo
     * @return
     */
    public final Boolean deleteCtr(Integer codigo)
    {
        return getInstanciaInformacao(codigo, null, null).exeDelete();
    }

    /**
     *
     * @param vf
     * @return
     */
    public final ArrayList<Object> getListaCtr(String... vf)
    {
        return getInstanciaInformacao(null, null, null).getLista(vf);
    }

    /**
     *
     * @param cbsistema
     * @param item o objeto do qual serão retirados os atributos para setar os
     * campos
     * @param txdescricao
     * @param outros na seguinte ordem \n codigo, nome, etc...
     */
    public abstract void setCampos(JFXComboBox cbsistema, JFXComboBox cbtipo, Spinner raridade, Object item, JFXTextArea txdescricao, JFXTextField... outros);

    protected abstract Informacao getInstanciaInformacao(Integer cod, Object sistema, Object tipo, String... vs);
}

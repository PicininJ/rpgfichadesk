/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AbstractOrInterface;

import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import javafx.collections.FXCollections;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableView;

/**
 *
 * @author Luish
 */
public abstract class CtrUsuario extends CtrBasica
{

    protected JFXTextField txcodigo;
    protected JFXTextField txnome;
    protected DatePicker dtcadastro;
    protected JFXTextField txemail;
    protected JFXPasswordField txPassword;
    protected TableView<Object> tabela;

    public void setALL(JFXTextField txcodigo, JFXTextField txnome, DatePicker dtcadastro, JFXTextField txemail, JFXPasswordField txPassword, TableView<Object> tabela)
    {
        this.txcodigo = txcodigo;
        this.txnome = txnome;
        this.dtcadastro = dtcadastro;
        this.txemail = txemail;
        this.txPassword = txPassword;
        this.tabela = tabela;
    }

    public void carregaTabela()
    {
        tabela.setItems(FXCollections.observableArrayList(getInstanciaInformacao().getLista()));
    }
    
}

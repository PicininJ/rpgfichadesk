package AbstractOrInterface;

import Entidades.Ficha;


public interface Dano
{
    public abstract double calcDano(Ficha atacante, Ficha defensor, int dadoAtaque, int dadoDefesa);
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AbstractOrInterface;

import Banco.Banco;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Luish
 */
public abstract class Basica
{
    private final boolean SQLexecuteProtected(String sql)
    {
        boolean f = false;
        try
        {
            Banco.getConexao().getConnection().setAutoCommit(false);
            if(( f = Banco.getConexao().manipular(sql)))
                Banco.getConexao().getConnection().commit();
            else
                Banco.getConexao().getConnection().rollback();
            Banco.getConexao().getConnection().setAutoCommit(true);
        } catch (SQLException ex)
        {
            f = false;
        }
        return f;
    }
    /**
     *Este método executa o SQL para a inserção do objeto ao banco de dados.
     * @return boolean informando se foi possivel realizar a inserção
     */
    public final boolean exeIncert()
    {
        String sql = insertSQL();
        return SQLexecuteProtected(sql);
    }

    /**
     *Este método executa o SQL para o update(alteração) do objeto ao banco  
     *de dados.
     * @return boolean informando se foi possivel realizar a alteração
     */
    public final boolean exeUpdate()
    {
        String sql = updateSQL();
        return SQLexecuteProtected(sql);
    }

    /**
     *Este método exclui o objeto da base de dados
     * @return boolean informando se foi possivel realizar a exclusão
     */
    public final boolean exeDelete()
    {
        String sql = deleteSQL();
        return SQLexecuteProtected(sql);
    }

    protected abstract String insertSQL();

    protected abstract String updateSQL();

    protected abstract String deleteSQL();

    /**
     *Este método retorna uma lista de objetos da base de dados
     * @param f filtro para filtrar as buscas na base de dados(não usado)
     * o mesmo é opcional(não prescisa ser passado por parametro)
     * @return Arraylist de objetos de acordo com sua instância
     */
    public final ArrayList<Object> getLista(String ...f)
    {
        ArrayList<Object> a = new ArrayList<>();            
        ResultSet rs = Banco.getConexao().consultar(montaConsultaSQL(f));
        try
        {
            while(rs.next())
                a.add(instanciaObjeto(rs));
            
        } catch (SQLException ex)
        {
            a = null;
        }
        return a;
    }

    protected abstract String montaConsultaSQL(String ...f);

    public abstract Object instanciaObjeto(ResultSet rs);
}

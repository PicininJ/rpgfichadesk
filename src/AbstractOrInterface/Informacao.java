package AbstractOrInterface;

import Banco.Banco;
import Entidades.SistemaRPG;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


public abstract class Informacao
{

    protected Integer codigo;

    protected String nome;

    protected String descricao;

    protected SistemaRPG sistema;

    public Informacao()
    {
    }
    
    

    public Informacao(Integer codigo, String nome, String descricao, SistemaRPG sistema)
    {
        this.codigo = codigo;
        this.nome = nome;
        this.descricao = descricao;
        this.sistema = sistema;
    }

    public Informacao(Integer codigo)
    {
        this.codigo = codigo;
    }

    public Informacao(String nome, String descricao, SistemaRPG sistema)
    {
        this.nome = nome;
        this.descricao = descricao;
        this.sistema = sistema;
    }

    public Integer getCodigo()
    {
        return codigo;
    }

    public void setCodigo(Integer codigo)
    {
        this.codigo = codigo;
    }

    public String getNome()
    {
        return nome;
    }

    public void setNome(String nome)
    {
        this.nome = nome;
    }

    public String getDescricao()
    {
        return descricao;
    }

    public void setDescricao(String descricao)
    {
        this.descricao = descricao;
    }

    public SistemaRPG getSistema()
    {
        return sistema;
    }

    public void setSistema(SistemaRPG sistema)
    {
        this.sistema = sistema;
    }

    private final boolean SQLexecuteProtected(String sql)
    {
        boolean f = false;
        try
        {
            Banco.getConexao().getConnection().setAutoCommit(false);
            if(( f = Banco.getConexao().manipular(sql)))
                Banco.getConexao().getConnection().commit();
            else
                Banco.getConexao().getConnection().rollback();
            Banco.getConexao().getConnection().setAutoCommit(true);
        } catch (SQLException ex)
        {
            f = false;
        }
        return f;
    }
    /**
     *Este método executa o SQL para a inserção do objeto ao banco de dados.
     * @return boolean informando se foi possivel realizar a inserção
     */
    public final boolean exeIncert()
    {
        String sql = insertSQL();
        return SQLexecuteProtected(sql);
    }

    /**
     *Este método executa o SQL para o update(alteração) do objeto ao banco  
     *de dados.
     * @return boolean informando se foi possivel realizar a alteração
     */
    public final boolean exeUpdate()
    {
        String sql = updateSQL();
        return SQLexecuteProtected(sql);
    }

    /**
     *Este método exclui o objeto da base de dados
     * @return boolean informando se foi possivel realizar a exclusão
     */
    public final boolean exeDelete()
    {
        String sql = deleteSQL();
        return SQLexecuteProtected(sql);
    }

    protected abstract String insertSQL();

    protected abstract String updateSQL();

    protected abstract String deleteSQL();

    /**
     *Este método retorna uma lista de objetos da base de dados
     * @param f filtro para filtrar as buscas na base de dados(não usado)
     * o mesmo é opcional(não prescisa ser passado por parametro)
     * @return Arraylist de objetos de acordo com sua instância
     */
    public final ArrayList<Object> getLista(String ...f)
    {
        ArrayList<Object> a = new ArrayList<>();            
        ResultSet rs = Banco.getConexao().consultar(montaConsultaSQL(f));
        try
        {
            while(rs.next())
                a.add(instanciaObjeto(rs));
        } catch (SQLException ex)
        {
            a = null;
        }
        return a;
    }

    protected abstract String montaConsultaSQL(String ...f);

    protected abstract Object instanciaObjeto(ResultSet rs);
    
    
}

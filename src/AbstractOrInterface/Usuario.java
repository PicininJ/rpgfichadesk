package AbstractOrInterface;

import Banco.Banco;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


public abstract class Usuario extends Basica
{

    protected Integer codigo;

    protected String nome;

    protected Date dtCadastro;

    public Usuario()
    {
    }

    public Usuario(String nome, Date dtCadastro)
    {
        this.nome = nome;
        this.dtCadastro = dtCadastro;
    }

    public Usuario(Integer codigo, String nome, Date dtCadastro)
    {
        this.codigo = codigo;
        this.nome = nome;
        this.dtCadastro = dtCadastro;
    }

    public Integer getCodigo()
    {
        return codigo;
    }

    public void setCodigo(Integer codigo)
    {
        this.codigo = codigo;
    }

    public String getNome()
    {
        return nome;
    }

    public void setNome(String nome)
    {
        this.nome = nome;
    }

    public Date getDtCadastro()
    {
        return dtCadastro;
    }

    public void setDtCadastro(Date dtCadastro)
    {
        this.dtCadastro = dtCadastro;
    }
}

package Entidades;

import AbstractOrInterface.Basica;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Culto extends Basica
{

    private Integer codigo;

    private String nome;

    private String devocao;
    
    private String descricao;
    
    private Regiao regiao;

    public Culto()
    {
    }

    public Culto(String nome, String devocao, String descricao, Regiao regiao)
    {
        this.nome = nome;
        this.devocao = devocao;
        this.descricao = descricao;
        this.regiao = regiao;
    }

    public Culto(Integer codigo, String nome, String devocao, String descricao, Regiao regiao)
    {
        this.codigo = codigo;
        this.nome = nome;
        this.devocao = devocao;
        this.descricao = descricao;
        this.regiao = regiao;
    }

    public Integer getCodigo()
    {
        return codigo;
    }

    public void setCodigo(Integer codigo)
    {
        this.codigo = codigo;
    }

    public String getNome()
    {
        return nome;
    }

    public void setNome(String nome)
    {
        this.nome = nome;
    }

    public String getDevocao()
    {
        return devocao;
    }

    public void setDevocao(String devocao)
    {
        this.devocao = devocao;
    }

    public String getDescricao()
    {
        return descricao;
    }

    public void setDescricao(String descricao)
    {
        this.descricao = descricao;
    }

    public Regiao getRegiao()
    {
        return regiao;
    }

    public void setRegiao(Regiao regiao)
    {
        this.regiao = regiao;
    }

    @Override
    protected String insertSQL()
    {
        return "INSERT INTO culto(cod_regiao, nome_culto, devocao_culto, descricao_culto) "
                + "VALUES("+regiao.getCodigo().toString()+", '"+nome+"', '"+devocao+"', '"+descricao+"')";
    }

    @Override
    protected String updateSQL()
    {
        return "UPDATE culto SET cod_regiao = "+regiao.getCodigo().toString()
                + ", nome_culto = '"+nome+"'"
                + ", devocao_culto = '"+devocao+"'"
                + ", descricao_culto = '"+descricao+"' "
                + "WHERE cod_culto = "+codigo.toString();
    }

    @Override
    protected String deleteSQL()
    {
        return "DELETE FROM culto WHERE cod_culto = "+codigo.toString();
    }

    @Override
    protected String montaConsultaSQL(String... f)
    {
        return "SELECT * FROM culto";
    }

    @Override
    public Object instanciaObjeto(ResultSet rs)
    {
        try
        {
            return new Culto(rs.getInt("cod_culto"), rs.getString("nome_culto"), rs.getString("devocao_culto"), rs.getString("descricao_culto"), new Regiao(rs.getInt("cod_regiao")));
        } catch (SQLException ex)
        {
            System.out.println("Erro intanciaobjeto na classe Culto: "+ex.getMessage());
            return null;
        }
    }

}

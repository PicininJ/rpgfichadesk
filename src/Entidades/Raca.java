package Entidades;

import AbstractOrInterface.Informacao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Raca extends Informacao
{

    public Raca(Integer codigo, String nome, String descricao, SistemaRPG sistema)
    {
        super(codigo, nome, descricao, sistema);
    }

    public Raca()
    {
    }

    public Raca(Integer codigo)
    {
        super(codigo);
    }

    public Raca(String nome, String descricao, SistemaRPG sistema)
    {
        super(nome, descricao, sistema);
    }

    

    @Override
    protected String insertSQL()
    {
        return "INSERT INTO raca(nome_raca, descricao_raca, cod_sistema) VALUES('"+nome+"', '"+descricao+"', "+sistema.getCodigo().toString()+")";
    }

    @Override
    protected String updateSQL()
    {
        return "UPDATE raca SET nome_raca = '"+nome+"', descricao_raca = '"+descricao+"', cod_sistema = "+sistema.getCodigo().toString()+" WHERE cod_raca = "+codigo.toString();
    }

    @Override
    protected String montaConsultaSQL(String ...f)
    {
        return "select * from raca";
    }

    @Override
    protected Object instanciaObjeto(ResultSet rs)
    {
        try
        {
            return new Raca(rs.getInt(1), rs.getString(2), rs.getString(3), new SistemaRPG(rs.getInt(4)));
        } catch (SQLException ex)
        {
            return null;
        }
    }

    @Override
    protected String deleteSQL()
    {
        return "DELETE FROM raca WHERE cod_raca = "+codigo.toString();
    }

}

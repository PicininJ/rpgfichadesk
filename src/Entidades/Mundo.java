package Entidades;

import AbstractOrInterface.Basica;
import Banco.Banco;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Mundo extends Basica
{

    private Integer codigo;

    private String nome;

    private String historia;

    private Mestre mestre;

    private ArrayList<Save> saves;

    public Mundo()
    {
        saves = new ArrayList<>();
    }

    /**
     *Construtor especial completa os demais atributos da classe(usar apenas em
     * situações em que não se tem acesso aos seus demais atributos no banco sem
     * que seja realizada uma busca) evitar seu uso quando já tiver os demais
     * atributos em memória
     * @param codigo código do mundo, atraves do codigo a propria classe 
     * preenchera os demais campos através de buscas executadas em seu interior 
     * com apenas o código do mundo passado como paramentro
     */
    public Mundo(Integer codigo)
    {
        this.codigo = codigo;
        saves = new ArrayList<>();
        completaCampos();
    }

    public Mundo(String nome, String historia, Mestre mestre)
    {
        this.nome = nome;
        this.historia = historia;
        this.mestre = mestre;
        saves = new ArrayList<>();
    }

    public Mundo(String nome, String historia, Mestre mestre, ArrayList<Save> saves)
    {
        this.nome = nome;
        this.historia = historia;
        this.mestre = mestre;
        this.saves = saves;
    }

    public Mundo(Integer codigo, String nome, String historia, Mestre mestre, ArrayList<Save> saves)
    {
        this.codigo = codigo;
        this.nome = nome;
        this.historia = historia;
        this.mestre = mestre;
        this.saves = saves;
    }

    public Integer getCodigo()
    {
        return codigo;
    }

    public void setCodigo(Integer codigo)
    {
        this.codigo = codigo;
    }

    public String getNome()
    {
        return nome;
    }

    public void setNome(String nome)
    {
        this.nome = nome;
    }

    public String getHistoria()
    {
        return historia;
    }

    public void setHistoria(String historia)
    {
        this.historia = historia;
    }

    public Mestre getMestre()
    {
        return mestre;
    }

    public void setMestre(Mestre mestre)
    {
        this.mestre = mestre;
    }

    public ArrayList<Save> getSaves()
    {
        return saves;
    }

    public void setSaves(ArrayList<Save> saves)
    {
        this.saves = saves;
    }

    @Override
    protected String insertSQL()
    {
        return "INSERT INTO mundo(nome_mundo, historia_mundo, cod_mestre) VALUES('"
                + nome + "', '" + historia + "', " + mestre.getCodigo().toString();
    }

    @Override
    protected String updateSQL()
    {
        return "UPDATE mundo SET nome_mundo = '" + nome
                + "', historia_mundo = '" + historia
                + "', cod_mestre = " + mestre.getCodigo().toString()
                + " WHERE cod_mundo = " + codigo.toString();
    }

    @Override
    protected String deleteSQL()
    {
        return "DELETE FORM mundo WHERE cod_mundo = " + codigo.toString();
    }

    @Override
    protected String montaConsultaSQL(String... f)
    {
        return "SELECT * FROM mundo";
    }

    @Override
    public Object instanciaObjeto(ResultSet rs)
    {
        try
        {
            return new Mundo(rs.getInt("cod_mundo"));
        } catch (SQLException ex)
        {
            System.out.println("Erro classe Mundo Instanciaobjeto: \n"+ex.getMessage());
            return null;
        }
    }

    private void completaCampos()
    {
        ResultSet r;
        ResultSet r2;

        r = Banco.getConexao().consultar("SELECT * FROM mundo where cod_mundo = " + codigo);

        r2 = Banco.getConexao().consultar("SELECT * FROM save where cod_mundo = " + codigo);

        try
        {
            while (r.next())
            {
                this.historia = r.getString("historia_mundo");
                this.nome = r.getString("nome_mundo");

                this.mestre = new Mestre();
                this.mestre.setCodigo(r.getInt("cod_mestre"));
            }
            
            while(r2.next())
            {
                saves.add((Save) new Save().instanciaObjeto(r2));
            }
            r.close();
            r2.close();
        } catch (SQLException ex)
        {
            System.out.println("Erro no autoi completar da classe Mundo:\n"+ex.getMessage());;
        }

    }

}

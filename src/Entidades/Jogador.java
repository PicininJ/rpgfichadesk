package Entidades;

import AbstractOrInterface.Usuario;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;


public class Jogador extends Usuario
{

    private Mundo mundo;
    public Jogador(Mundo mundo)
    {
        this.mundo = mundo;
    }
    
    public Jogador(Mundo mundo, String nome, Date dtCadastro)
    {
        super(nome, dtCadastro);
        this.mundo = mundo;
    }

    public Jogador(Mundo mundo, Integer codigo, String nome, Date dtCadastro)
    {
        super(codigo, nome, dtCadastro);
        this.mundo = mundo;
    }
    
    @Override
    protected String insertSQL()
    {
        return "INSERT INTO jogador(nome_jogador, dt_cadastro_jogador, cod_mundo) "
                + "VALUES('"+nome+"', '"+dtCadastro.toString()+"', "+mundo.getCodigo().toString()+")";
    }

    @Override
    protected String updateSQL()
    {
        return "UPDATE jogador SET nome_jogador = '"+nome+"'"
                + ", dt_cadastro_jogador = '"+dtCadastro.toString()+"'"
                + ", cod_mundo = "+mundo.getCodigo().toString()+" WHERE cod_jogador = "+codigo.toString();
    }

    @Override
    protected String deleteSQL()
    {
        return "DELETE FROM jogador WHERE cod_jogador = "+codigo.toString();
    }

    @Override
    protected String montaConsultaSQL(String... f)
    {
        return "SELECT * FROM jogador";
    }

    @Override
    public Object instanciaObjeto(ResultSet rs)
    {
        try
        {
            return new Jogador(new Mundo(rs.getInt("cod_mundo")), rs.getInt("cod_jogador"), rs.getString("nome_jogador"), rs.getDate("dt_cadastro_jogador"));
        } catch (SQLException ex)
        {
            System.out.println("Erro na instancia classe jogador!!!");
            return null;
        }
       
    }
    
}

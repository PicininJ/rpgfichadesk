package Entidades;

import AbstractOrInterface.Informacao;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Desvantagem extends Informacao
{

    public Desvantagem()
    {
    }

    public Desvantagem(String nome, String descricao, SistemaRPG sistema)
    {
        super(nome, descricao, sistema);
    }

    public Desvantagem(Integer codigo, String nome, String descricao, SistemaRPG sistema)
    {
        super(codigo, nome, descricao, sistema);
    }

    @Override
    protected String insertSQL()
    {
        return "INSERT INTO desvantagem(nome_desvantagem, descricao_desvantagem, cod_sistema) "
                + "VALUES('"+nome+"', '"+descricao+"', "+sistema.getCodigo().toString()+")";
    }

    @Override
    protected String updateSQL()
    {
        return "UPDATE desvantagem SET nome_desvantagem = '"+nome
                +"', descricao_desvantagem = '"+descricao
                +"', cod_sistema = "+sistema.getCodigo().toString()
                +" WHERE cod_desvantagem = "+codigo.toString();
    }

    @Override
    protected String montaConsultaSQL(String ...f)
    {
        return "select * from desvantagem";
    }

    @Override
    protected Object instanciaObjeto(ResultSet rs)
    {
        try
        {
            return new Desvantagem(rs.getInt(1), rs.getString(2), rs.getString(3), new SistemaRPG(rs.getInt(4)));
        } catch (SQLException ex)
        {
            return null;
        }
    }

    @Override
    protected String deleteSQL()
    {
        return "DELETE FROM desvantagem WHERE cod_desvantagem = "+codigo.toString();
    }

}

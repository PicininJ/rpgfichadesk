package Entidades;

import AbstractOrInterface.Informacao;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Vantagem extends Informacao
{

    public Vantagem(Integer codigo, String nome, String descricao, SistemaRPG sistema)
    {
        super(codigo, nome, descricao, sistema);
    }

    public Vantagem()
    {
    }

    public Vantagem(String nome, String descricao, SistemaRPG sistema)
    {
        super(nome, descricao, sistema);
    }

    @Override
    protected String insertSQL()
    {
        return "INSERT INTO vantagem(nome_vantagem, descricao_vantagem, cod_sistema) "
                + "VALUES('"+nome+"', '"+descricao+"', "+sistema.getCodigo().toString()+")";
    }

    @Override
    protected String updateSQL()
    {
        return "UPDATE vantagem SET nome_vantagem = '"+nome+"', descricao_vantagem = '"+descricao+"', cod_sistema = "+sistema.getCodigo().toString()+" WHERE cod_vantagem = "+codigo.toString();
    }

    @Override
    protected String montaConsultaSQL(String ...f)
    {
        return "select * from vantagem";
    }

    @Override
    protected Object instanciaObjeto(ResultSet rs)
    {
        try
        {
            return new Vantagem(rs.getInt(1), rs.getString(2), rs.getString(3), new SistemaRPG(rs.getInt(4)));
        } catch (SQLException ex)
        {
            return null;
        }
    }

    @Override
    protected String deleteSQL()
    {
        return "DELETE FROM vantagem WHERE cod_vantagem = "+codigo.toString();
    }

}

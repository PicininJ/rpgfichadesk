package Entidades;

import AbstractOrInterface.Dano;
import AbstractOrInterface.Dano;
import java.io.Serializable;
import java.sql.ResultSet;
import java.util.ArrayList;
import org.json.JSONObject;


public class Ficha implements Serializable
{

    protected Integer cod;

    protected String nome;

    protected Integer dinheiro;

    protected Integer hp;

    protected Integer mp;

    protected Integer xp;

    protected Integer lvl;

    protected String status;

    protected Integer mpMax;

    protected Integer hpMax;

    protected Integer tipo_ficha;

    protected Integer sorte;
    
    protected Dano dano;
    
    protected Jogador jogador;
    
    private JSONObject atr_dinamicos;

    public Ficha()
    {
        
    }
    /////////////////////
    
    public Integer getCod()
    {
        return cod;
    }

    public void setCod(Integer cod)
    {
        this.cod = cod;
    }

    public String getNome()
    {
        return nome;
    }

    public void setNome(String nome)
    {
        this.nome = nome;
    }

    public Integer getDinheiro()
    {
        return dinheiro;
    }

    public void setDinheiro(Integer dinheiro)
    {
        this.dinheiro = dinheiro;
    }

    public Integer getHp()
    {
        return hp;
    }

    public void setHp(Integer hp)
    {
        this.hp = hp;
    }

    public Integer getMp()
    {
        return mp;
    }

    public void setMp(Integer mp)
    {
        this.mp = mp;
    }

    public Integer getXp()
    {
        return xp;
    }

    public void setXp(Integer xp)
    {
        this.xp = xp;
    }

    public Integer getLvl()
    {
        return lvl;
    }

    public void setLvl(Integer lvl)
    {
        this.lvl = lvl;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public Integer getMpMax()
    {
        return mpMax;
    }

    public void setMpMax(Integer mpMax)
    {
        this.mpMax = mpMax;
    }

    public Integer getHpMax()
    {
        return hpMax;
    }

    public void setHpMax(Integer hpMax)
    {
        this.hpMax = hpMax;
    }

    public Integer getTipo_ficha()
    {
        return tipo_ficha;
    }

    public void setTipo_ficha(Integer tipo_ficha)
    {
        this.tipo_ficha = tipo_ficha;
    }

    public Integer getSorte()
    {
        return sorte;
    }

    public void setSorte(Integer sorte)
    {
        this.sorte = sorte;
    }

    public Dano getDano()
    {
        return dano;
    }

    public void setDano(Dano dano)
    {
        this.dano = dano;
    }

    public Jogador getJogador()
    {
        return jogador;
    }

    public void setJogador(Jogador jogador)
    {
        this.jogador = jogador;
    }

    public JSONObject getAtr_dinamicos()
    {
        return atr_dinamicos;
    }

    public void setAtr_dinamicos(JSONObject atr_dinamicos)
    {
        this.atr_dinamicos = atr_dinamicos;
    }
    ////////////////////

    public final boolean exeIncert()
    {
        return false;
    }
    
    public final boolean exeUpdate()
    {
        return false;
    }
    
    public final boolean exeDelete()
    {
        return false;
    }
    
    protected String insertSQL()
    {
        return "";
    }
    
    protected String updateSQL()
    {
        return "";
    }
    
    protected String deleteSQL()
    {
        return "";
    }
    
    protected String montaConsultaSQL()
    {
        return "";
    }
    
    public Object instanciaObjeto(ResultSet r)
    {
        return null;
    }
    
    public final ArrayList<Object> getLista()
    {
        return null;
    }
}

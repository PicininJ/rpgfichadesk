package Entidades;

import AbstractOrInterface.Basica;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Guilda extends Basica
{

    private Integer codigo;

    private String nome;

    private String descicao;

    private Cidade cidade;

    public Guilda()
    {
    }

    public Guilda(String nome, String descicao, Cidade cidade)
    {
        this.nome = nome;
        this.descicao = descicao;
        this.cidade = cidade;
    }

    public Guilda(Integer codigo, String nome, String descicao, Cidade cidade)
    {
        this.codigo = codigo;
        this.nome = nome;
        this.descicao = descicao;
        this.cidade = cidade;
    }

    
    public Integer getCodigo()
    {
        return codigo;
    }

    public void setCodigo(Integer codigo)
    {
        this.codigo = codigo;
    }

    public Cidade getCidade()
    {
        return cidade;
    }

    public void setCidade(Cidade cidade)
    {
        this.cidade = cidade;
    }

    @Override
    protected String insertSQL()
    {
        return "INSERT INTO guilda(nome_guilda, cod_cidade, descricao_guilda) "
                + "VALUES('" + nome + "', " + cidade.getCodigo() + ", '" + descicao + "')";
    }

    @Override
    protected String updateSQL()
    {
        return "UPDATE guilda SET nome_guilda = '" + nome
                + "', cod_cidade = " + cidade.getCodigo()
                + ", descricao_guilda = '" + descicao + "' WHERE cod_guilda = " + codigo;
    }

    @Override
    protected String deleteSQL()
    {
        return "DELETE FROM guilda WHERE cod_guilda = " + codigo;
    }

    @Override
    protected String montaConsultaSQL(String... f)
    {
        return "SELECT * FROM guilda";
    }

    @Override
    public Object instanciaObjeto(ResultSet rs)
    {
        try
        {
            return new Guilda(rs.getInt("cod_guilda"), rs.getString("nome_guilda"), rs.getString("descricao_guilda"), new Cidade(rs.getInt("cod_cidade")));
        } catch (SQLException ex)
        {
            System.out.println("Erro instancia objeto classe Guilda");
            return null;
        }
    }

}

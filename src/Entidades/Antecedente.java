package Entidades;

import AbstractOrInterface.Informacao;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Antecedente extends Informacao
{

    public Antecedente(Integer codigo, String nome, String descricao, SistemaRPG sistema)
    {
        super(codigo, nome, descricao, sistema);
    }

    public Antecedente(String nome, String descricao, SistemaRPG sistema)
    {
        super(nome, descricao, sistema);
    }

    public Antecedente()
    {
    }

    @Override
    protected String insertSQL()
    {
        return "INSERT INTO antecedentes(nome_antecedente, descricao_antecedente, cod_sistema) "
                + "VALUES('"+nome
                +"', '"+descricao
                +"', "+sistema.getCodigo().toString()+")";
    }

    @Override
    protected String updateSQL()
    {
        return "UPDATE antecedentes SET nome_antecedente = '"+nome
                +"', descricao_antecedente = '"+descricao
                +"', cod_sistema = "+sistema.getCodigo().toString()
                +" WHERE cod_antecedente = "+codigo.toString();
    }

    @Override
    protected String montaConsultaSQL(String ...f)
    {
        return "select * from antecedentes";
    }

    @Override
    protected Object instanciaObjeto(ResultSet rs)
    {
        try
        {
            return new Antecedente(rs.getInt(1), rs.getString(2), rs.getString(3), new SistemaRPG(rs.getInt(4)));
        } catch (SQLException ex)
        {
            return null;
        }
    }

    @Override
    protected String deleteSQL()
    {
        return "DELETE FROM antecedentes WHERE cod_antecedente = "+codigo.toString();
    }

}

package Entidades;

import AbstractOrInterface.Basica;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Save extends Basica
{

    private Integer codigo;

    private Date dtSave;

    private String descricao;
    
    private Mundo mundo;

    public Save()
    {
    }

    public Save(Date dtSave, String descricao)
    {
        this.dtSave = dtSave;
        this.descricao = descricao;
    }

    public Save(Date dtSave, String descricao, Mundo mundo)
    {
        this.dtSave = dtSave;
        this.descricao = descricao;
        this.mundo = mundo;
    }

    public Save(Integer codigo, Date dtSave, String descricao, Mundo mundo)
    {
        this.codigo = codigo;
        this.dtSave = dtSave;
        this.descricao = descricao;
        this.mundo = mundo;
    }

    public Integer getCodigo()
    {
        return codigo;
    }

    public void setCodigo(Integer codigo)
    {
        this.codigo = codigo;
    }

    public Date getDtSave()
    {
        return dtSave;
    }

    public void setDtSave(Date dtSave)
    {
        this.dtSave = dtSave;
    }

    public String getDescricao()
    {
        return descricao;
    }

    public void setDescricao(String descricao)
    {
        this.descricao = descricao;
    }

    public Mundo getMundo()
    {
        return mundo;
    }

    public void setMundo(Mundo mundo)
    {
        this.mundo = mundo;
    }

    @Override
    protected String insertSQL()
    {
        return "INSERT INTO save(cod_mundo, dt_save, descricao_save) VALUES("+mundo.getCodigo()+", '"+dtSave.toString()+"', "+descricao+")";
    }

    @Override
    protected String updateSQL()
    {
        return "UPDATE save SET cod_mundo = "+mundo.getCodigo()
                + ", dt_save = '"+dtSave.toString()
                + "', descricao_save = '"+descricao+"' WHERE cod_save = "+codigo.toString();
    }

    @Override
    protected String deleteSQL()
    {
        return "DELETE FROM save WHERE cod_save = "+codigo.toString();
    }

    @Override
    protected String montaConsultaSQL(String... f)
    {
        return "SELECT * FROM save";
    }

    @Override
    public Object instanciaObjeto(ResultSet rs)
    {
        try
        {
            Mundo m = new Mundo();
            m.setCodigo(rs.getInt("cod_mundo"));
            return new Save(rs.getInt("cod_save"), rs.getDate("dt_save"), rs.getString("descricao_save"), m);
        } catch (Exception ex)
        {
            System.out.println("Erro na instancia do Save:\n"+ex.getMessage());
            return null;
        }
    }
    

}

package Entidades;

import AbstractOrInterface.Informacao;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Habilidade extends Informacao
{

    public Habilidade(Integer codigo, String nome, String descricao, SistemaRPG sistema)
    {
        super(codigo, nome, descricao, sistema);
    }

    public Habilidade()
    {
    }

    public Habilidade(String nome, String descricao, SistemaRPG sistema)
    {
        super(nome, descricao, sistema);
    }

    @Override
    protected String insertSQL()
    {
        return "INSERT INTO habilidade(nome_habilidade, descricao_habilidade, cod_sistema) "
                + "VALUES('"+nome+"', '"+descricao+"', "+sistema.getCodigo().toString()+")";
    }

    @Override
    protected String updateSQL()
    {
        return "UPDATE habilidade SET nome_habilidade = '"+nome
                +"', descricao_habilidade = '"+descricao
                +"', cod_sistema = "+sistema.getCodigo().toString()
                +" WHERE cod_habilidade = "+codigo.toString();
    }

    @Override
    protected String montaConsultaSQL(String ...f)
    {
        return "select * from habilidade";
    }

    @Override
    protected Object instanciaObjeto(ResultSet rs)
    {
        try
        {
            return new Habilidade(rs.getInt(1), rs.getString(2), rs.getString(3), new SistemaRPG(rs.getInt(4)));
        } catch (SQLException ex)
        {
            return null;
        }
    }

    @Override
    protected String deleteSQL()
    {
        return "DELETE FROM habilidade WHERE cod_habilidade = "+codigo.toString();
    }

}

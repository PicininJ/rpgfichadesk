package Entidades;

import AbstractOrInterface.Basica;
import Banco.Banco;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


public class Masmorra extends Basica
{

    private Integer codigo;
    private String nome;
    private String historia;
    private Integer lvl;
    private Regiao regiao;
    private ArrayList<Ficha> fichas;

    public Masmorra()
    {
    }

    public Masmorra(Integer codigo)
    {
        this.codigo = codigo;
    }

    public Masmorra(Integer codigo, String nome, String historia, Integer lvl, Regiao regiao)
    {
        this.codigo = codigo;
        this.nome = nome;
        this.historia = historia;
        this.lvl = lvl;
        this.regiao = regiao;
    }

    public Masmorra(Integer codigo, String nome, String historia, Integer lvl, Regiao regiao, ArrayList<Ficha> fichas)
    {
        this.codigo = codigo;
        this.nome = nome;
        this.historia = historia;
        this.lvl = lvl;
        this.regiao = regiao;
        this.fichas = fichas;
    }

    public String getHistoria()
    {
        return historia;
    }

    public void setHistoria(String historia)
    {
        this.historia = historia;
    }

    public ArrayList<Ficha> getFichas()
    {
        return fichas;
    }

    public void setFichas(ArrayList<Ficha> fichas)
    {
        this.fichas = fichas;
    }

    public Integer getCodigo()
    {
        return codigo;
    }

    public void setCodigo(Integer codigo)
    {
        this.codigo = codigo;
    }

    public String getNome()
    {
        return nome;
    }

    public void setNome(String nome)
    {
        this.nome = nome;
    }

    public Integer getLvl()
    {
        return lvl;
    }

    public void setLvl(Integer lvl)
    {
        this.lvl = lvl;
    }

    public Regiao getRegiao()
    {
        return regiao;
    }

    public void setRegiao(Regiao regiao)
    {
        this.regiao = regiao;
    }

    @Override
    protected String insertSQL()
    {
        return "INSERT INTO masmorra(historia, nome_masmorra, lvl_masmorra, cod_regiao) "
                + "VALUES('"+historia+"', '"+nome+"', "+lvl.toString()+", "+regiao.getCodigo().toString()+")";
    }

    @Override
    protected String updateSQL()
    {
        return "UPDATE masmorra SET historia = '"+historia
                + "', nome_masmorra = '"+nome
                + "', lvl_masmorra = "+lvl.toString()
                + ", cod_regiao = "+regiao.getCodigo().toString()
                +" WHERE cod_masmorra = "+codigo.toString();
    }

    @Override
    protected String deleteSQL()
    {
        return "DELETE FROM masmorra WHERE cod_masmorra = "+codigo.toString();
    }

    @Override
    protected String montaConsultaSQL(String... f)
    {
        return "SELECT * FROM masmorra";
    }

    @Override
    public Object instanciaObjeto(ResultSet rs)
    {
        Masmorra m = null;
        Ficha f = new Ficha();///tratar diferenca quando as classes ficha estiverem implementadas, irá variar de acordo com o sistema
        ArrayList<Ficha> fs = new ArrayList<>();
        try
        {
            m = new Masmorra(rs.getInt("cod_masmorra"), rs.getString("nome_masmorra"), rs.getString("historia"), rs.getInt("lvl_masmorra"), new Regiao(rs.getInt("cod_regiao")));
            ResultSet rf = Banco.getConexao().consultar("SELECT * FROM ficha INNER JOIN masmorra_ficha ON ficha.cod_ficha = masmorra_ficha.cod_ficha AND masmorra_ficha.cod_masmorra = " + m.getCodigo().toString());
            while (rf.next())
            {
                fs.add((Ficha) f.instanciaObjeto(rf));
            }
            m.setFichas(fs);

        } catch (SQLException ex)
        {
            m = null;
        }
        return m;
    }
    
    
}

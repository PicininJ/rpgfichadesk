package Entidades;

import AbstractOrInterface.Basica;
import Banco.Banco;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


public class Regiao extends Basica
{
    private Integer codigo;
    private String nome;
    private String historia;
    private ArrayList<Cidade> cidades;
    private ArrayList<Masmorra> masmorras;
    private ArrayList<Culto> cultos;

    public Regiao()
    {
    }

    public Regiao(Integer codigo)
    {
        this.codigo = codigo;
        autoCompletar();
    }

    public Regiao(String nome, String historia)
    {
        this.nome = nome;
        this.historia = historia;
    }

    public Regiao(Integer codigo, String nome, String historia)
    {
        this.codigo = codigo;
        this.nome = nome;
        this.historia = historia;
    }

    public Regiao(Integer codigo, String nome, String historia, ArrayList<Cidade> cidades, ArrayList<Masmorra> masmorras)
    {
        this.codigo = codigo;
        this.nome = nome;
        this.historia = historia;
        this.cidades = cidades;
        this.masmorras = masmorras;
    }

    public Integer getCodigo()
    {
        return codigo;
    }

    public void setCodigo(Integer codigo)
    {
        this.codigo = codigo;
    }

    public String getNome()
    {
        return nome;
    }

    public void setNome(String nome)
    {
        this.nome = nome;
    }

    public ArrayList<Cidade> getCidades()
    {
        return cidades;
    }

    public void setCidades(ArrayList<Cidade> cidades)
    {
        this.cidades = cidades;
    }

    public ArrayList<Masmorra> getMasmorras()
    {
        return masmorras;
    }

    public void setMasmorras(ArrayList<Masmorra> masmorras)
    {
        this.masmorras = masmorras;
    }

    public String getHistoria()
    {
        return historia;
    }

    public void setHistoria(String historia)
    {
        this.historia = historia;
    }

    @Override
    protected String insertSQL()
    {
        return "INSERT INTO regiao(historia, nome) "
                + "VALUES('"+historia+"', '"+nome+"')";
    }

    @Override
    protected String updateSQL()
    {
        return "UPDATE regiao SET historia = '"+historia
                + "', nome = '"+nome+"' WHERE cod_regiao = "+codigo.toString();
    }

    @Override
    protected String deleteSQL()
    {
        return "SELECT * FROM regiao WHERE cod_regiao = "+codigo;
    }

    @Override
    protected String montaConsultaSQL(String... f)
    {
        return "SELECT * FROM regiao";
    }

    @Override
    public Object instanciaObjeto(ResultSet rs)
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void autoCompletar()
    {
        ResultSet r = Banco.getConexao().consultar("SELECT * FROM regiao WHERE cod_regiao = "+codigo.toString());
        try
        {
            while(r.next())            
            {
                this.historia = r.getString("historia_regiao");
                this.nome = r.getString("nome_regiao");
            }
        } catch (SQLException ex)
        {
            System.out.println("Erro auto completar classe Regiao: \n"+ex.getMessage());
        }
    }
    
    
}

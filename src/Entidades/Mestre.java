package Entidades;

import AbstractOrInterface.Basica;
import AbstractOrInterface.Usuario;
import Banco.Banco;
import java.security.MessageDigest;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Mestre extends Usuario
{

    private String senha;

    private String email;

    private ArrayList<Mundo> mundos;

    public Mestre()
    {
    }

    
    public Mestre(String senha, String email, ArrayList<Mundo> mundos, String nome, Date dtCadastro)
    {
        super(nome, dtCadastro);
        this.senha = senha;
        this.email = email;
        this.mundos = mundos;
    }

    public Mestre(String senha, String email, ArrayList<Mundo> mundos, Integer codigo, String nome, Date dtCadastro)
    {
        super(codigo, nome, dtCadastro);
        this.senha = senha;
        this.email = email;
        this.mundos = mundos;
    }

    public String getSenha()
    {
        return senha;
    }

    public void setSenha(String senha)
    {
        this.senha = senha;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public ArrayList<Mundo> getMundos()
    {
        return mundos;
    }

    public void setMundos(ArrayList<Mundo> mundos)
    {
        this.mundos = mundos;
    }

    @Override
    protected String insertSQL()
    {
        String password = encripto(senha);
        return "INSERT INTO mestre(senha_mestre, nome_mestre, email_mestre, dt_cadastro) VALUES('"+password+"', '"+nome+"', '"+email+"', '"+dtCadastro.toString()+"')";
    }

    @Override
    protected String updateSQL()
    {
        String password = encripto(senha);
        return "UPDATE mestre SET senha_mestre = '"+password
                + "', nome_mestre = '"+nome
                + "', email_mestre = '"+email
                + "', dt_cadastro = '"+dtCadastro.toString()+"'";
    }

    @Override
    protected String deleteSQL()
    {
        return "DELETE FROM mestre WHERE cod_mestre = "+codigo.toString();
    }

    @Override
    protected String montaConsultaSQL(String... f)
    {
        return "SELECT * FROM mestre";
    }

    @Override
    public Object instanciaObjeto(ResultSet rs)
    {
        try
        {
            ArrayList<Mundo> ms = new ArrayList<>();
            ResultSet rm = Banco.getConexao().consultar("SELECT * FROM mundo WHERE cod_mestre = "+Integer.toString(rs.getInt("cod_mestre")));
            while(rm.next())
            {
                ms.add(new Mundo(rm.getInt("nome_mundo")));
            }
            return new Mestre(rs.getString("senha_mestre"), rs.getString("email_mestre"), ms, rs.getInt("cod_mestre"), rs.getString("nome_mestre"), rs.getDate("dt_cadastro"));
        } catch (SQLException ex)
        {
            return null;
        }
    }
    private String encripto(String s)
    {
        String password = new String(s);
        try
        {

            MessageDigest algorithm = MessageDigest.getInstance("SHA-256");
            byte messageDigest[] = algorithm.digest(s.getBytes("UTF-8"));
            password = new String(messageDigest);

        } catch (Exception ex)
        {
            throw new UnsupportedOperationException("Não foi possivel encriptografar a senha do mestre, operação cancelada!!!"); //To change body of generated methods, choose Tools | Templates.
        }
        return password;
    }
}

package Entidades;

import AbstractOrInterface.Basica;
import Banco.Banco;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Cidade extends Basica
{

    private Integer codigo;
    private String nome;
    private String historia;
    private Regiao regiao;
    private ArrayList<Guilda> guildas;

    public Cidade()
    {
    }

    public Cidade(Integer codigo)
    {
        this.codigo = codigo;
        autoCompletar();
    }

    public Cidade(Integer codigo, String nome, String historia, Regiao regiao)
    {
        this.codigo = codigo;
        this.nome = nome;
        this.historia = historia;
        this.regiao = regiao;
    }

    public Cidade(String nome, String historia, Regiao regiao, ArrayList<Guilda> guildas)
    {
        this.nome = nome;
        this.historia = historia;
        this.regiao = regiao;
        this.guildas = guildas;
    }

    public Cidade(Integer codigo, String nome, String historia, Regiao regiao, ArrayList<Guilda> guildas)
    {
        this.codigo = codigo;
        this.nome = nome;
        this.historia = historia;
        this.regiao = regiao;
        this.guildas = guildas;
    }

    public Integer getCodigo()
    {
        return codigo;
    }

    public void setCodigo(Integer codigo)
    {
        this.codigo = codigo;
    }

    public String getNome()
    {
        return nome;
    }

    public void setNome(String nome)
    {
        this.nome = nome;
    }

    public String getHistoria()
    {
        return historia;
    }

    public void setHistoria(String historia)
    {
        this.historia = historia;
    }

    public Regiao getRegiao()
    {
        return regiao;
    }

    public void setRegiao(Regiao regiao)
    {
        this.regiao = regiao;
    }

    public ArrayList<Guilda> getGuildas()
    {
        return guildas;
    }

    public void setGuildas(ArrayList<Guilda> guildas)
    {
        this.guildas = guildas;
    }

    @Override
    protected String insertSQL()
    {
        return "INSERT INTO cidade(cod_regiao, nome, historia) "
                + "VALUES(" + regiao.getCodigo().toString() + ", '" + nome + "', '" + historia + "')";
    }

    @Override
    protected String updateSQL()
    {
        return "UPDATE cidade SET cod_regiao = " + regiao.getCodigo().toString()
                + ", nome = '" + nome
                + "', historia = '" + historia
                + "' WHERE cod_cidade = " + codigo.toString();
    }

    @Override
    protected String deleteSQL()
    {
        return "DELETE FROM cidade WHERE cod_cidade = " + codigo.toString();
    }

    @Override
    protected String montaConsultaSQL(String... f)
    {
        return "SELECT * FROM cidade";
    }

    @Override
    public Object instanciaObjeto(ResultSet rs)
    {
        Cidade c = null;
        Guilda g = new Guilda();
        ArrayList<Guilda> gs = new ArrayList<>();
        try
        {
            c = new Cidade(rs.getInt("cod_cidade"), rs.getString("nome"), rs.getString("historia"), new Regiao(rs.getInt("cod_regiao")), new ArrayList<Guilda>());
            ResultSet rg = Banco.getConexao().consultar("SELECT * FROM guilda WHERE cod_cidade = " + c.getCodigo().toString());
            while (rg.next())
            {
                gs.add((Guilda) g.instanciaObjeto(rg));
            }
            c.setGuildas(gs);

        } catch (SQLException ex)
        {
            c = null;
        }
        return c;
    }

    private void autoCompletar()
    {
        ResultSet r = Banco.getConexao().consultar("SELECT * FROM cidade WHERE cod_cidade = " + codigo.toString());
        try
        {
            while (r.next())
            {
                this.historia = r.getString("historia");
                this.nome = r.getString("nome");
                this.regiao = new Regiao(r.getInt("cod_regiao"));
            }
        } catch (SQLException ex)
        {
            System.out.println("Erro auto completar classe Cidade: \n" + ex.getMessage());
        }
    }
}

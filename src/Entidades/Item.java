package Entidades;

import AbstractOrInterface.Informacao;
import java.sql.ResultSet;
import java.sql.SQLException;


public class Item extends Informacao
{

    private Integer raridade;//corrigir depois
    private Tipo tipo;

    public Item(Integer codigo, String nome, String descricao, SistemaRPG sistema)
    {
        super(codigo, nome, descricao, sistema);
    }

    public Item()
    {
    }

    public Item(Integer codigo)
    {
        super(codigo);
    }

    public Item(String nome, String descricao, SistemaRPG sistema)
    {
        super(nome, descricao, sistema);
    }

    public Item( Integer codigo, String nome, String descricao,Integer raridade, Tipo tipo, SistemaRPG sistema)
    {
        super(codigo, nome, descricao, sistema);
        this.raridade = raridade;
        this.tipo = tipo;
    }

    

    
    @Override
    protected String insertSQL()
    {
        return "INSERT INTO item(nome_item, descricao_item, cod_sistema, raridade, cod_tipo) "
                + "VALUES('"+nome+"', '"+descricao+"', "
                +sistema.getCodigo().toString()+", "+raridade.toString()+", "+tipo.getCodigo().toString()+")";
    }

    @Override
    protected String updateSQL()
    {
        return "UPDATE item SET nome_item = '"+nome
                +"', descricao_item = '"+descricao
                +"', cod_sistema = "+sistema.getCodigo().toString()
                +", raridade = "+raridade.toString()
                +", cod_tipo = "+tipo.getCodigo().toString()
                +" WHERE cod_item = "+codigo.toString();
    }

    @Override
    protected String montaConsultaSQL(String ...f)
    {
        return "select * from item";
    }

    @Override
    protected Object instanciaObjeto(ResultSet rs)
    {
        try
        {
            return new Item(rs.getInt("cod_item"), rs.getString("nome_item"), 
                    rs.getString("descricao_item"), rs.getInt("raridade"), 
                    new Tipo(rs.getInt("cod_tipo")), 
                    new SistemaRPG(rs.getInt("cod_sistema")));
        } catch (SQLException ex)
        {
            return null;
        }
    }

    @Override
    protected String deleteSQL()
    {
        return "DELETE FROM item WHERE cod_item = "+codigo.toString();
    }

    public Integer getRaridade()
    {
        return raridade;
    }

    public void setRaridade(Integer raridade)
    {
        this.raridade = raridade;
    }

    public Tipo getTipo()
    {
        return tipo;
    }

    public void setTipo(Tipo tipo)
    {
        this.tipo = tipo;
    }

}

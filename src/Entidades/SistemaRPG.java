package Entidades;

import Banco.Banco;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


public class SistemaRPG
{

    private Integer codigo;

    private String nome;
    
    private String descricao;

    public SistemaRPG()
    {
    }

    
    public SistemaRPG(Integer codigo, String nome)
    {
        this.codigo = codigo;
        this.nome = nome;
    }

    public SistemaRPG(Integer codigo, String nome, String descricao)
    {
        this.codigo = codigo;
        this.nome = nome;
        this.descricao = descricao;
    }

    public SistemaRPG(String nome, String descricao)
    {
        this.nome = nome;
        this.descricao = descricao;
    }

    public SistemaRPG(Integer codigo)
    {
        this.codigo = codigo;
        ResultSet rs = Banco.getConexao().consultar("SELECT * FROM sistemarpg WHERE cod_sistema = "+this.codigo.toString());
        try
        {
            while(rs.next())
            {
                this.nome = rs.getString("nome_sistema");
                this.descricao = rs.getString("descricao_sistema");
            }
        } catch (SQLException ex)
        {
            
        }
    }

    public Integer getCodigo()
    {
        return codigo;
    }

    public void setCodigo(Integer codigo)
    {
        this.codigo = codigo;
    }

    public String getNome()
    {
        return nome;
    }

    public void setNome(String nome)
    {
        this.nome = nome;
    }

    public String getDescricao()
    {
        return descricao;
    }

    public void setDescricao(String descricao)
    {
        this.descricao = descricao;
    }

    @Override
    public String toString()
    {
        return nome;
    }

    public ArrayList<Object> getLista(String[] vf)
    {
        ArrayList<Object> a = new ArrayList<>();
        ResultSet rs = Banco.getConexao().consultar("SELECT * FROM sistemarpg");
        try
        {
            while(rs.next())
            {
                a.add(new SistemaRPG(rs.getInt("cod_sistema"), rs.getString("nome_sistema"), rs.getString("descricao_sistema")));
            }
        } catch (SQLException ex)
        {
            a.clear();
        }
        return a;
    }
    
    public boolean insert()
    {
        String sql = "INSERT INTO sistemarpg(nome_sistema, descricao_sistema) VALUES('"+nome+"', '"+descricao+"')";
        return Banco.getConexao().manipular(sql);
    }
    public boolean update()
    {
        String sql = "UPDATE sistemarpg SET nome_sistema = '"+nome
                + "', descricao_sistema = '"+descricao+"' WHERE cod_sistema = "+codigo.toString();
        return Banco.getConexao().manipular(sql);
    }
    public boolean delete()
    {
        String sql = "DELETE FROM sistemarpg WHERE cod_sistema = "+codigo.toString();
        return Banco.getConexao().manipular(sql);
    }
}

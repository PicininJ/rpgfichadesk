package Entidades;

import AbstractOrInterface.Informacao;
import Banco.Banco;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Tipo extends Informacao
{

    public Tipo(Integer codigo, String nome, String descricao, SistemaRPG sistema)
    {
        super(codigo, nome, descricao, sistema);
    }

    public Tipo(String nome, String descricao, SistemaRPG sistema)
    {
        super(nome, descricao, sistema);
    }
    
    public Tipo(Integer codigo)
    {
        super(codigo);
        completaCampos();
    }

    public Tipo()
    {
    }
    
    
    @Override
    protected String insertSQL()
    {
        return "INSERT INTO tipo(nome_tipo, descricao_tipo, cod_sistema) VALUES('"+nome+"', '"+descricao+"', "+sistema.getCodigo().toString()+")";
    }

    @Override
    protected String updateSQL()
    {
        return "UPDATE tipo SET nome_tipo = '"+nome
                +"', descricao_tipo = '"+descricao
                +"', cod_sistema = "+sistema.getCodigo().toString()
                +" WHERE cod_tipo = "+codigo.toString();
    }

    @Override
    protected String montaConsultaSQL(String ...f)
    {
        return "select * from tipo";
    }

    @Override
    protected Object instanciaObjeto(ResultSet rs)
    {
        try
        {
            return new Tipo(rs.getInt(1), rs.getString(2), rs.getString(3), new SistemaRPG(rs.getInt(4)));
        } catch (SQLException ex)
        {
            return null;
        }
    }

    @Override
    protected String deleteSQL()
    {
        return "DELETE FROM tipo WHERE cod_tipo = "+codigo.toString();
    }

    private void completaCampos()
    {
        ResultSet rs = Banco.getConexao().consultar(montaConsultaSQL("select * from tipo WHERE cod_tipo = "+codigo));
        Tipo t = null;
        try
        {
            while(rs.next())
                t = (Tipo) (instanciaObjeto(rs));
            
        } catch (SQLException ex)
        {
            System.out.println("Erro classe Tipo");
            t = new Tipo(codigo);
        }
        this.descricao = t.getDescricao();
        this.nome = t.getNome();
        this.sistema = t.getSistema();
    }

    @Override
    public String toString()
    {
        return nome;
    }

    @Override
    public boolean equals(Object obj)
    {
        return (obj instanceof Tipo) && this.codigo == ((Tipo)obj).getCodigo();
    }

}

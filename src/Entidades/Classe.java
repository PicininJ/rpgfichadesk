/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import AbstractOrInterface.Informacao;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Aluno
 */
public class Classe extends Informacao
{

    public Classe(Integer codigo, String nome, String descricao, SistemaRPG sistema)
    {
        super(codigo, nome, descricao, sistema);
    }

    public Classe(String nome, String descricao, SistemaRPG sistema)
    {
        super(nome, descricao, sistema);
    }

    @Override
    protected String insertSQL()
    {
        return "INSERT INTO classe(nome_classe, descricao_classe, cod_sistema) "
                + "VALUES('" + nome + "', '" + descricao + "', " + sistema.getCodigo().toString() + ")";
    }

    @Override
    protected String updateSQL()
    {
        return "UPDATE classe SET nome_classe = '"+nome
                +"', descricao_classe = '"+descricao
                +"', cod_sistema = "+sistema.getCodigo().toString()
                +" WHERE cod_antecedente = "+codigo.toString();
    }

    @Override
    protected String montaConsultaSQL(String... f)
    {
        return "select * from classe";
    }

    @Override
    protected Object instanciaObjeto(ResultSet rs)
    {
        try
        {
            return new Classe(rs.getInt(1), rs.getString(2), rs.getString(3), new SistemaRPG(rs.getInt(4)));
        } catch (SQLException ex)
        {
            return null;
        }
    }

    @Override
    protected String deleteSQL()
    {
        return "DELETE FROM classe WHERE cod_classe = "+codigo.toString();
    }

}

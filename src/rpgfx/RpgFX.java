/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rpgfx;

import Banco.Banco;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author Luish
 */
public class RpgFX extends Application
{

    private static Stage STAGE;

    public static Stage getSTAGE()
    {
        return STAGE;
    }

    public static void setSTAGE(Stage STAGE)
    {
        RpgFX.STAGE = STAGE;
    }

    @Override
    public void start(Stage stage) throws Exception
    {
        STAGE = stage;
        Parent root = FXMLLoader.load(getClass().getResource("/GUI/TelaLogin.fxml"));
        
        
        Scene scene = new Scene(root);
        
        
        STAGE.setScene(scene);
        if (Banco.conectar())
        {
            STAGE.show();
        } else
        {
            System.exit(0);
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        launch(args);
    }

}

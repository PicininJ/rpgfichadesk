/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladoras;

import AbstractOrInterface.Basica;
import AbstractOrInterface.CtrBasica;
import Entidades.Cidade;
import Entidades.Masmorra;
import Entidades.Regiao;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import java.util.ArrayList;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.scene.control.ListView;

/**
 *
 * @author Luish
 */
public class CtrRegiao extends CtrBasica
{

    /**
     * ordem: codigo, nome, história, cidades e masmorra
     *
     * @param o lista de elementos da tela
     */
    @Override
    public void setCampos(Object... o)
    {
        JFXTextField cod = (JFXTextField) o[0];
        JFXTextField no = (JFXTextField) o[1];
        JFXTextArea his = (JFXTextArea) o[2];
        ListView<Object> cidades = (ListView<Object>) o[3];
        ListView<Object> masmorras = (ListView<Object>) o[4];
        
        Regiao r = (Regiao) o[5];
        cod.setText(r.getCodigo().toString());
        no.setText(r.getNome());
        his.setText(r.getHistoria());
        cidades.setItems(FXCollections.observableArrayList(r.getCidades()));
        masmorras.setItems(FXCollections.observableArrayList(r.getMasmorras()));
        
    }

    /**
     * ordem: codigo, nome, história, cidades e masmorra
     *
     * @param o lista de elementos da tela
     * @return a instancia da classe correta
     */
    @Override
    protected Basica getInstanciaInformacao(Object... o)
    {
        try
        {
            Integer cod = (Integer) o[0];
            String nome = (String) o[1];
            String his = (String) o[2];
            List<Cidade> cs = (List<Cidade>) o[3];
            List<Masmorra> ms = (List<Masmorra>) o[4];

            return new Regiao(cod, nome, his, (ArrayList<Cidade>) cs, (ArrayList<Masmorra>) ms);
        }
        catch(Exception ex)
        {
            System.out.println("erro de instancia CtrRegiao: \n"+ex.getMessage());
            return new Regiao();
        }
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladoras;

import AbstractOrInterface.CtrInformacao;
import AbstractOrInterface.Informacao;
import Entidades.Item;
import Entidades.SistemaRPG;
import Entidades.Tipo;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Spinner;

/**
 *
 * @author Luish
 */
public class CtrItem extends CtrInformacao
{

    @Override
    public void setCampos(JFXComboBox cbsistema, JFXComboBox cbtipo, Spinner raridade, Object item, JFXTextArea txdescricao, JFXTextField... outros)
    {
        if(item != null)
        {
            Item i = (Item) item;
            
            outros[0].setText(i.getCodigo().toString());
            outros[1].setText(i.getNome());
            //outros[2].setText(i.getRaridade().toString());
            raridade.getValueFactory().setValue(i.getRaridade());
            txdescricao.setText(i.getDescricao());
           
            cbsistema.getItems().clear();
            cbsistema.getItems().add(i.getSistema());
            cbsistema.getSelectionModel().selectFirst();
            
            cbtipo.getSelectionModel().select(cbtipo.getItems().indexOf(i.getTipo()));
        }
    }

    @Override
    protected Informacao getInstanciaInformacao(Integer cod, Object sistema,Object tipo, String... vs)
    {
        if(vs.length > 0)
            return new Item(cod, vs[0], vs[1], Integer.parseInt(vs[2]), (Tipo) tipo, (SistemaRPG) sistema);
        else if(cod != null)
        {
            Item i = new Item();
            i.setCodigo(cod);
            return i;
        }
        else
            return new Item();
    }
    public static ObservableList<Object> getTiposItens()
    {
        return FXCollections.observableArrayList(new Tipo().getLista());
    }
}

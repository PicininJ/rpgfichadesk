/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladoras;

import AbstractOrInterface.CtrInformacao;
import AbstractOrInterface.Informacao;
import Entidades.Antecedente;
import Entidades.SistemaRPG;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import javafx.scene.control.Spinner;

/**
 *
 * @author Luish
 */
public class CtrAntecedente extends CtrInformacao
{

    @Override
    public void setCampos(JFXComboBox cbsistema, JFXComboBox cbtipo, Spinner raridade, Object item, JFXTextArea txdescricao, JFXTextField... outros)
    {
        if(item != null)
        {
            Antecedente t = (Antecedente) item;
            
            outros[0].setText(t.getCodigo().toString());
            outros[1].setText(t.getNome());
            txdescricao.setText(t.getDescricao());
           
            cbsistema.getItems().clear();
            cbsistema.getItems().add(t.getSistema());
            cbsistema.getSelectionModel().selectFirst();
        }
    }

    @Override
    protected Informacao getInstanciaInformacao(Integer cod, Object sistema,Object tipo, String... vs)
    {
        if(vs.length > 0)
            return new Antecedente(cod, vs[0], vs[1], (SistemaRPG) sistema);
        else if(cod != null)
        {
            Antecedente i = new Antecedente();
            i.setCodigo(cod);
            return i;
        }
        else
            return new Antecedente();
    }
    
}

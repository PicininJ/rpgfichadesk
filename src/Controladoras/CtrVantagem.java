/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladoras;

import AbstractOrInterface.CtrInformacao;
import AbstractOrInterface.Informacao;
import Entidades.SistemaRPG;
import Entidades.Vantagem;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import javafx.scene.control.Spinner;

/**
 *
 * @author Luish
 */
public class CtrVantagem extends CtrInformacao
{

    @Override
    public void setCampos(JFXComboBox cbsistema, JFXComboBox cbtipo, Spinner raridade, Object item, JFXTextArea txdescricao, JFXTextField... outros)
    {
        if(item != null)
        {
            Vantagem v = (Vantagem) item;
            
            outros[0].setText(v.getCodigo().toString());
            outros[1].setText(v.getNome());
            txdescricao.setText(v.getDescricao());
           
            cbsistema.getItems().clear();
            cbsistema.getItems().add(v.getSistema());
            cbsistema.getSelectionModel().selectFirst();
        }
    }

    @Override
    protected Informacao getInstanciaInformacao(Integer cod, Object sistema,Object tipo, String... vs)
    {
        if(vs.length > 0)
            return new Vantagem(cod, vs[0], vs[1], (SistemaRPG) sistema);
        else if(cod != null)
        {
            Vantagem v = new Vantagem();
            v.setCodigo(cod);
            return v;
        }
        else
            return new Vantagem();
    }
}

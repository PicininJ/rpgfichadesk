/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladoras;

import AbstractOrInterface.CtrInformacao;
import AbstractOrInterface.Informacao;
import Entidades.SistemaRPG;
import Entidades.Tipo;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import javafx.scene.control.Spinner;

/**
 *
 * @author Luish
 */
public class CtrTipo extends CtrInformacao
{

    @Override
    public void setCampos(JFXComboBox cbsistema, JFXComboBox cbtipo, Spinner raridade, Object item, JFXTextArea txdescricao, JFXTextField... outros)
    {
        if(item != null)
        {
            Tipo t = (Tipo) item;
            
            outros[0].setText(t.getCodigo().toString());
            outros[1].setText(t.getNome());
            txdescricao.setText(t.getDescricao());
           
            cbsistema.getItems().clear();
            cbsistema.getItems().add(t.getSistema());
            cbsistema.getSelectionModel().selectFirst();
        }
    }

    @Override
    protected Informacao getInstanciaInformacao(Integer cod, Object sistema,Object tipo, String... vs)
    {
        if(vs.length > 0)
            return new Tipo(cod, vs[0], vs[1], (SistemaRPG) sistema);
        else if(cod != null)
        {
            Tipo i = new Tipo();
            i.setCodigo(cod);
            return i;
        }
        else
            return new Tipo();
    }
    
}

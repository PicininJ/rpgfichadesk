/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladoras;

import AbstractOrInterface.CtrInformacao;
import AbstractOrInterface.Informacao;
import Entidades.Desvantagem;
import Entidades.SistemaRPG;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import java.util.ArrayList;
import javafx.scene.control.Spinner;

/**
 *
 * @author Luish
 */
public class CtrDesvantagem extends CtrInformacao
{

    @Override
    public void setCampos(JFXComboBox cbsistema, JFXComboBox cbtipo, Spinner raridade, Object item, JFXTextArea txdescricao, JFXTextField... outros)
    {
        if(item != null)
        {
            Desvantagem d = (Desvantagem) item;
            
            outros[0].setText(d.getCodigo().toString());
            outros[1].setText(d.getNome());
            txdescricao.setText(d.getDescricao());
           
            cbsistema.getItems().clear();
            cbsistema.getItems().add(d.getSistema());
            cbsistema.getSelectionModel().selectFirst();
        }
    }

    @Override
    protected Informacao getInstanciaInformacao(Integer cod, Object sistema,Object tipo, String... vs)
    {
        if(vs.length > 0)
            return new Desvantagem(cod, vs[0], vs[1], (SistemaRPG) sistema);
        else if(cod != null)
        {
            Desvantagem d = new Desvantagem();
            d.setCodigo(cod);
            return d;
        }
        else
            return new Desvantagem();
    }

    

    /*public Boolean insertCtr(String nome, String descricao, Object sistema)
    {
        if(sistema != null)
            return new Desvantagem(nome, descricao, (SistemaRPG) sistema).exeIncert();
        else
            return false;
    }

    public Boolean updateCtr(Integer codigo, String nome, String descricao, Object sistema)
    {
        return new Desvantagem(codigo, nome, descricao, (SistemaRPG) sistema).exeUpdate();
    }

    public Boolean deleteCtr(Integer codigo)
    {
        Desvantagem d = new Desvantagem();
        d.setCodigo(codigo);
        return d.exeDelete();
    }

    public ArrayList<Object> getListaCtr(String... vf)
    {
        return new Desvantagem().getLista(vf);
    }

    public void setCampos(JFXTextField txcodigo, JFXTextField txnome, JFXTextArea txdescricao, JFXComboBox cbsistema, Object item)
    {
        if(item != null)
        {
            Desvantagem d = (Desvantagem) item;
            
            txcodigo.setText(d.getCodigo().toString());
            txnome.setText(d.getNome());
            txdescricao.setText(d.getDescricao());
           
            cbsistema.getItems().clear();
            cbsistema.getItems().add(d.getSistema());
            cbsistema.getSelectionModel().selectFirst();
        }
    }*/

}

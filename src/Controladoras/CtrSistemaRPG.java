/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladoras;

import Entidades.SistemaRPG;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import java.util.ArrayList;

/**
 *
 * @author Luish
 */
public class CtrSistemaRPG
{

    public static Boolean insertCtr(String nome, String descricao)
    {
        return new SistemaRPG(nome, descricao).insert();
    }

    public static Boolean updateCtr(Integer codigo, String nome, String descricao)
    {
        return new SistemaRPG(codigo, nome, descricao).update();
    }

    public static Boolean deleteCtr(Integer codigo)
    {
        return new SistemaRPG(codigo).delete();
    }

    public static ArrayList<Object> getListaCtr(String... vf)
    {
        return new SistemaRPG().getLista(vf);
    }

    public static void preencheCampos(JFXTextField txcodigo, JFXTextField txnome, JFXTextArea txdescricao, Object selectedItem)
    {
        SistemaRPG s = (SistemaRPG) selectedItem;
        txcodigo.setText(s.getCodigo().toString());
        txnome.setText(s.getNome());
        txdescricao.setText(s.getDescricao());
    }

    public void setCampos(Object... o)
    {
        SistemaRPG s = (SistemaRPG) o[o.length];
        ((JFXTextField)o[0]).setText(s.getCodigo().toString());
        ((JFXTextField)o[1]).setText(s.getNome());
        ((JFXTextField)o[2]).setText(s.getDescricao());
    }
}

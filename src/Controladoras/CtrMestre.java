/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladoras;

import AbstractOrInterface.Basica;
import AbstractOrInterface.CtrUsuario;
import Entidades.Mestre;
import java.sql.Date;

/**
 *
 * @author Luish
 */
public class CtrMestre extends CtrUsuario
{

    @Override
    public void setCampos(Object... o)
    {
        Mestre m = (Mestre) tabela.getSelectionModel().getSelectedItem();
        if(m != null)
        {
            txcodigo.setText(m.getCodigo().toString());
            txnome.setText(m.getNome());
            txemail.setText(m.getEmail());
            txPassword.setText(m.getSenha());
        }
    }

    @Override
    protected Basica getInstanciaInformacao(Object... o)
    {
        //(String senha, String email, ArrayList<Mundo> mundos, Integer codigo, String nome, Date dtCadastro)
        return new Mestre(txPassword.getText(), 
                txemail.getText(),  null, (txcodigo.getText().isEmpty() ? null : Integer.parseInt(txcodigo.getText())), 
                txnome.getText(), Date.valueOf(dtcadastro.getValue()));
    }
    
}
